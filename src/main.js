import Vue from 'vue';
// import App from './App';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

const Home = {template:'<div>Home page</div>'};
const About = {template:'<div>About page</div>'};
const Contact = {template:'<div>Contact page</div>'};

const router = new VueRouter({
  mode:'history',
  base: __dirname,
  routes:[
    {path:'/',component:Home},
    {path:'/about',component:About},
    {path:'/contact',component:Contact}
  ]
})





new Vue({
  el:'#app',
  router,
  template:`<div>
    <ul>

<li><router-link to="/">Home</router-link></li>
<li><router-link to="/about">About</router-link></li>
<li><router-link to="/contact">Contact</router-link></li>

    </ul>
<router-view></router-view>
  </div>`
})
